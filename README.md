# mathem delivery stuff
### erik stefan haglund

## running the app
### prerequisites
`npm install`

### start frontend
`npm start`, should open http://localhost:3000/

## tests
`npm test`

## how to use
Enter zip code, select some products, click Check dates.

Click on a date to log a delivery for that zip code, on that date.

While using the frontend, the business logic output can be viewed in the browser console.
