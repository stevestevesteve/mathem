const deliveryPeriod = (startDate, length) => {
	const dates = [];

	for (let i = 0; i < length; ++i) {
		dates.push(startDate.clone().add(i, 'd'));
	}

	return dates;
};

module.exports = { deliveryPeriod };
