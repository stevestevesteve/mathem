const productById = (products, id) => products.find(product => product.productId === id);

module.exports = { productById };
