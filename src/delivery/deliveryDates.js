const { productById } = require('./productById');
const { productDeliveryDates } = require('./productDeliveryDates');

// Pretty basic here, would normally be in a db or whatever,
// be cached, etc
const getProducts = () => require('../data/products.json');

const deliveryDates = (productIds, period) => {
	const products = productIds.map(id => productById(getProducts(), id)).filter(product => !!product);
	
	let dates = period;
	products.forEach(product => {
		if (dates.length > 0) {
			dates = productDeliveryDates(product, dates);
		}
	});

	return dates;
};

module.exports = { deliveryDates };