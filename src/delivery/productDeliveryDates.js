/*
	Takes a product definition and a period of time, and eliminates
	dates which don't follow the given rules
*/

const { dayRule } = require('./rules/dayRule');
const { externalProductRule } = require('./rules/externalProductRule');
const { temporaryProductRule } = require('./rules/temporaryProductRule');
const { advanceDaysRule } = require('./rules/advanceDaysRule');

const RULES = [
	dayRule,
	externalProductRule,
	temporaryProductRule,
	advanceDaysRule,
];

// Rules have a common API so this part of the code is completely
// rule-agnostic
const productDeliveryDates = (product, period) => {
	let dates = period;

	const startDate = period[0].clone();
	RULES.forEach(rule => dates = rule({ product, period: dates, startDate }));

	return dates;
};

module.exports = { productDeliveryDates };
