/*
	Takes a range of dates and eliminates the ones which are not
	in the same week as the startDate
*/

// same clone issue as externalProductRule.js
const temporaryProductRule = ({ product, period, startDate }) =>
	product.productType === 'temporary'
	? period.filter(date => date.isoWeek() === startDate.isoWeek())
	: period;

module.exports = { temporaryProductRule };
