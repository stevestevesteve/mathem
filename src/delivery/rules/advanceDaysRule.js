/*
	Takes a range of dates and eliminates the ones which are not
	at least 'advanceDaysAhead' days ahead
*/

// same clone issue as externalProductRule.js
const advanceDaysRule = ({ product, period, startDate }) =>
	period.filter(date => {
		const earliestDate = startDate.clone().add(product.daysInAdvance, 'd');

		return date.isSame(earliestDate) || date.isAfter(earliestDate);
	});

module.exports = { advanceDaysRule };
