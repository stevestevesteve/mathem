/*
	Takes a range of dates and eliminates the ones which don't fall
	on some given days of the week
*/

const dayRule = ({ period, product }) => period.filter(date => product.deliveryDays.includes(date.isoWeekday()));

module.exports = { dayRule };
