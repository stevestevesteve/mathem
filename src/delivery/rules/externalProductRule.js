/*
	Takes a range of dates and eliminates the ones which are not
	at least 5 days ahead
*/

// Here the startDate is cloned for every day in the period, which isn't great
// see more notes on a possible 'rule' API in productDeliveryDates.js
const externalProductRule = ({ product, period, startDate }) => 	
	product.productType === 'external'
	? period.filter(date => {
		const earliestDate = startDate.clone().add(5, 'd');
		return date.isSame(earliestDate) || date.isAfter(earliestDate);
	})
	: period;


module.exports = { externalProductRule };
