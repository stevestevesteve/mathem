const moment = require('moment');

const { REFERENCE_MONDAY } = require('../../test/days');
const { deliveryDates } = require('./deliveryDates');
const { deliveryPeriod } = require('./deliveryPeriod');

const TWO_WEEKS = deliveryPeriod(moment(REFERENCE_MONDAY), 14);

test('Bananas can be delivered Monday-Friday', () => {
	const dates = deliveryDates([ 1 ], TWO_WEEKS);

	expect(dates).toHaveLength(10);

	[
		'2019-06-10',
		'2019-06-11',
		'2019-06-12',
		'2019-06-13',
		'2019-06-14',

		'2019-06-17',
		'2019-06-18',
		'2019-06-19',
		'2019-06-20',
		'2019-06-21',
	].forEach((date, i) => {
		expect(dates[i].format('YYYY-MM-DD')).toBe(date);
	});
});

test('Bananas & pancakes can only be delivered Thursdays of the same week', () => {
	const dates = deliveryDates([1, 6], TWO_WEEKS);

	expect(dates).toHaveLength(1);

	[
		'2019-06-13',
	].forEach((date, i) => {
		expect(dates[i].format('YYYY-MM-DD')).toBe(date);
	});
});

test('Given external thing & bird cages can only be delivered Wednesdays and Fridays, starting 5 days ahead', () => {
	const dates = deliveryDates([4, 8], TWO_WEEKS);

	expect(dates).toHaveLength(2);

	[
		'2019-06-19',
		'2019-06-21',
	].forEach((date, i) => {
		expect(dates[i].format('YYYY-MM-DD')).toBe(date);
	});
});
