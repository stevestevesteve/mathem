const { isGreen, saveDelivery } = require('./deliveries');

const ZIP_CODE = '11122';
const DATE = '2019-01-01';

test('Should not be green if there is no delivery', () => {
	expect(isGreen(ZIP_CODE, DATE)).toBe(false);
});

test('Should be green after a delivery is scheduled', () => {
	saveDelivery(ZIP_CODE, DATE);
	expect(isGreen(ZIP_CODE, DATE)).toBe(true);
});
