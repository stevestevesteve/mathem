const moment = require('moment');

const { REFERENCE_MONDAY } = require('../../test/days');
const { productByName } = require('../../test/productByName');

const { deliveryPeriod } = require('./deliveryPeriod');
const { productDeliveryDates } = require('./productDeliveryDates');

const TWO_WEEKS = deliveryPeriod(moment(REFERENCE_MONDAY), 14);

test('Bananas can be delivered Monday-Friday', () => {
	const dates = productDeliveryDates(productByName('Banana'), TWO_WEEKS);

	expect(dates).toHaveLength(10);

	[
		'2019-06-10',
		'2019-06-11',
		'2019-06-12',
		'2019-06-13',
		'2019-06-14',

		'2019-06-17',
		'2019-06-18',
		'2019-06-19',
		'2019-06-20',
		'2019-06-21',
	].forEach((date, i) => {
		expect(dates[i].format('YYYY-MM-DD')).toBe(date);
	});
});

test('Given external product can be delivered any day, starting 5 days ahead', () => {
	const dates = productDeliveryDates(productByName('Something not in stock'), TWO_WEEKS);

	expect(dates).toHaveLength(9);

	[
		'2019-06-15',
		'2019-06-16',
		'2019-06-17',
		'2019-06-18',
		'2019-06-19',
		'2019-06-20',
		'2019-06-21',
		'2019-06-22',
		'2019-06-23',
	].forEach((date, i) => {
		expect(dates[i].format('YYYY-MM-DD')).toBe(date);
	});
});

test('Given temporary product can be delivered Thursdays in current week', () => {
	const dates = productDeliveryDates(productByName('Discount Pancakes (5)'), TWO_WEEKS);

	expect(dates).toHaveLength(1);
	expect(dates[0].format('YYYY-MM-DD')).toBe('2019-06-13');
});

test('Products with daysInAdvance can only be delivered some days later', () => {
	const dates = productDeliveryDates(productByName('Fresh bread'), TWO_WEEKS);

	expect(dates).toHaveLength(8);

	[
		'2019-06-12',
		'2019-06-13',
		'2019-06-14',

		'2019-06-17',
		'2019-06-18',
		'2019-06-19',
		'2019-06-20',
		'2019-06-21',
	].forEach((date, i) => {
		expect(dates[i].format('YYYY-MM-DD')).toBe(date);
	});
});

