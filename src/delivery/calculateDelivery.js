const moment = require('moment');

const { deliveryPeriod } = require('./deliveryPeriod');
const { deliveryDates } = require('./deliveryDates');
const { isGreen } = require('./deliveries');
const { sortGreenDelivery } = require('./sortGreenDelivery');

const TWO_WEEKS_DAYS = 14;

const calculateDelivery = (zipCode, productIds, startDate = moment()) => {
	// really bad catch-all error handling, each part should have its own
	// error handling with reasonable messages, codes, whatever you want
	//
	// but I've decided to skip it due to time contraints
	try {
		const greenSorter = sortGreenDelivery(startDate);

		const period = deliveryPeriod(startDate, TWO_WEEKS_DAYS);
		const dates = deliveryDates(productIds, period);
		const mappedDates = dates.map(date => ({
			postalCode: zipCode,
			deliveryDate: date.format(),
			isGreenDelivery: isGreen(zipCode, date.format('YYYY-MM-DD')),
		}));
		mappedDates.sort(greenSorter);

		return {
			success: dates.length > 0,
			data: {
				deliveryDates: mappedDates,
			},
		};
	} catch(e) {
		return {
			success: false,
			error: e.message,
		};
	}
};

module.exports = { calculateDelivery };