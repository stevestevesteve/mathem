const moment = require('moment');

const { REFERENCE_MONDAY } = require('../../test/days');
const { deliveryPeriod } = require('./deliveryPeriod');

test('The correct delivery period should be calculated', () => {
	const oneDay = deliveryPeriod(moment(REFERENCE_MONDAY), 1);

	expect(oneDay).toHaveLength(1);
	expect(oneDay[0].format('YYYY-MM-DD')).toBe('2019-06-10');

	const twoWeeks = deliveryPeriod(moment(REFERENCE_MONDAY), 14);

	expect(twoWeeks).toHaveLength(14);
	[
		'2019-06-10',
		'2019-06-11',
		'2019-06-12',
		'2019-06-13',
		'2019-06-14',
		'2019-06-15',
		'2019-06-16',

		'2019-06-17',
		'2019-06-18',
		'2019-06-19',
		'2019-06-20',
		'2019-06-21',
		'2019-06-22',
		'2019-06-23',
	].forEach((date, i) => expect(twoWeeks[i].format('YYYY-MM-DD')).toBe(date));
});