const moment = require('moment');

const { REFERENCE_MONDAY } = require('../../test/days');
const { calculateDelivery } = require('./calculateDelivery');
const { saveDelivery } = require('./deliveries');

const ZIPCODE = '12345';

test('Bananas can be delivered Monday-Friday, success is true, zip code is returned, deliveries are not green', () => {
	const result = calculateDelivery(ZIPCODE, [1], moment(REFERENCE_MONDAY));

	expect(result.success).toBe(true);
	expect(result.data.deliveryDates).toHaveLength(10);

	for(let i = 0; i < 10; ++i) {
		expect(result.data.deliveryDates[i].postalCode).toBe(ZIPCODE);
		expect(result.data.deliveryDates[i].isGreenDelivery).toBe(false);
	}
});

test('isGreenDelivery should be true on date with saved delivery', () => {
	saveDelivery(ZIPCODE, REFERENCE_MONDAY);

	const result = calculateDelivery(ZIPCODE, [1], moment(REFERENCE_MONDAY));

	expect(result.success).toBe(true);
	expect(result.data.deliveryDates[0].isGreenDelivery).toBe(true);
	expect(result.data.deliveryDates[1].isGreenDelivery).toBe(false);
});

test('Success should be false if no dates were found', () => {
	// Products 6, 7 are incompatible for delivery
	const result = calculateDelivery(ZIPCODE, [6, 7], moment(REFERENCE_MONDAY));

	expect(result.success).toBe(false);
	expect(result.data.deliveryDates).toHaveLength(0);
});

test('Green deliveries within 3 days are sorted on top', () => {
	const  ZIPCODE_2 = '98765';

	// within 3 days
	saveDelivery(ZIPCODE_2, '2019-06-11');
	saveDelivery(ZIPCODE_2, '2019-06-12');
	saveDelivery(ZIPCODE_2, '2019-06-13');

	// not within 3 days
	saveDelivery(ZIPCODE_2, '2019-06-20');

	const GREEN_DATES = [0, 1, 2, 8];
	const isGreen = (index) => GREEN_DATES.includes(index);

	const result = calculateDelivery(ZIPCODE_2, [1], moment(REFERENCE_MONDAY));

	[
		'2019-06-11T00:00:00+02:00',
		'2019-06-12T00:00:00+02:00',
		'2019-06-13T00:00:00+02:00',
		'2019-06-10T00:00:00+02:00',
		'2019-06-14T00:00:00+02:00',

		'2019-06-17T00:00:00+02:00',
		'2019-06-18T00:00:00+02:00',
		'2019-06-19T00:00:00+02:00',
		'2019-06-20T00:00:00+02:00',
		'2019-06-21T00:00:00+02:00',
	].forEach((date, i) => {
		expect(result.data.deliveryDates[i].deliveryDate).toBe(date);
		expect(result.data.deliveryDates[i].isGreenDelivery).toBe(isGreen(i));
	});
});
