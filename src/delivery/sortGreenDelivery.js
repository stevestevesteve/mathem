const moment = require('moment');

const isPriority = (isGreen, momentDate, priorityDate) =>
	isGreen &&
	(momentDate.isBefore(priorityDate) || momentDate.isSame(priorityDate));

const sortGreenDelivery = (startDate) => (a, b) => {
	const dateA = moment(a.deliveryDate);
	const dateB = moment(b.deliveryDate);
	
	if (a.isGreenDelivery !== b.isGreenDelivery) {
		// if only one delivery is green, check if it's within
		// the priority period
		//
		// not sure if 'within 3 days' means days 1..3, or day 1 + 3,
		// here I'm going with +3
		const lastPriorityDate = startDate.clone().add(3, 'd');

		if (isPriority(a.isGreenDelivery, dateA, lastPriorityDate)) {
			return -1;
		}
		else if (isPriority(b.isGreenDelivery, dateB, lastPriorityDate)) {
			return 1;
		}
	}

	// in other caes, compare normally
	if (dateA.isBefore(dateB)) {
		return -1;
	} else if (dateA.isAfter(dateB)) {
		return 1;
	}

	return 0;
};

module.exports = { sortGreenDelivery };
