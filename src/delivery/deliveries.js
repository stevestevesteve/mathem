const pendingDeliveries = { };

// Really nothing special here, only implemented to show the isGreenDelivery flag
// this data should be saved to/retrieved from some database in real life use
const isGreen = (zipCode, date) => {
	const deliveries = pendingDeliveries[zipCode] || [];
	return deliveries.includes(date);
};

const saveDelivery = (zipCode, date) => {
	if (!isGreen(zipCode, date)) {
		const deliveries = pendingDeliveries[zipCode] || [];
		if (!deliveries.includes(date)) {
			deliveries.push(date);
			pendingDeliveries[zipCode] = deliveries;
		}
	}
}

module.exports = { isGreen, saveDelivery };
