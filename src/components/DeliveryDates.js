import React from 'react';
import moment from 'moment';

import { saveDelivery } from '../delivery/deliveries';

const DeliveryDates = ({ zipCode, success, dates, datePicked }) => {
	if (!success) {
		return <div>No valid dates for delivery were found.</div>
	}

	return (
		<ul style={{ listStyle: 'none' }}>
			{dates.map(date => {
				const dateShort = moment(date.deliveryDate).format('YYYY-MM-DD');
				const save = () => {
					saveDelivery(date.postalCode, dateShort);
					datePicked();
				}

				return (
					<li key={`deliver-${dateShort}`}>
						<button
							style={date.isGreenDelivery ? { backgroundColor: '#66ce5f' } : { backgroundColor: '#417bd8' }}
							onClick={save}>{dateShort}
						</button>
					</li>
				);
			})}
		</ul>);
}

export default DeliveryDates;
