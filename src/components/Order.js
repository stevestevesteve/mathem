import React, { useState } from 'react';

import ProductList from './ProductList';
import DeliveryDates from './DeliveryDates';
import { calculateDelivery } from '../delivery/calculateDelivery';

const BIG = { fontSize: 20 };

const Order = () => {
	const [zipCode, setZipCode] = useState('');
	const [products, setProducts] = useState([]);
	const [dates, setDates] = useState([]);
	const [success, setSuccess] = useState(undefined);

	const datePicked = () => {
		setDates([]);
		setSuccess(undefined);
		setProducts([]);
	};

	const addProduct = (id) => {
		setProducts([...products, id]);
	};

	const removeProduct = (id) => {
		setProducts(products.filter(pId => pId !== id));
	};

	const fetchDates = () => {
		// The checkbox value fields turn the ids into strings,
		// this conversion could also happen in changeProduct()
		// in ProductList.js
		const productIds = products.map(id => parseInt(id, 10));

		const response = calculateDelivery(zipCode, productIds);

		// log output for easy viewing in console
		console.log(response);
		
		setDates(response.data.deliveryDates);
		setSuccess(response.success);
	};

	return (
		<>
			<input style={BIG} value={zipCode} onChange={e => setZipCode(e.target.value)} placeholder="Enter zip code" />
			<ProductList products={products} addProduct={addProduct} removeProduct={removeProduct} />
			<button style={BIG} onClick={fetchDates} disabled={zipCode.length === 0}>Check dates</button>
			{success !== undefined && (
				<DeliveryDates zipCode={zipCode} success={success} dates={dates} datePicked={datePicked} />
			)}
		</>
	);
};

export default Order;
