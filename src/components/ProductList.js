import React from 'react';

import productList from '../data/products.json';

const ProductList = ({ products, addProduct, removeProduct }) => {
	const changeProduct = (e) => {
		if (e.target.checked) {
			addProduct(e.target.value);
		} else {
			removeProduct(e.target.value);
		}
	};

	return (
		<ul style={{ listStyle: 'none' }}>
			{productList.map(product => (
				<li key={product.productId} >
					<input
						type="checkbox"
						id={product.productId}
						value={product.productId}
						onChange={changeProduct}
						checked={products.includes(`${product.productId}`)}
					/>
					<label htmlFor={product.productId}>{product.name}</label>
				</li>
			))}
		</ul>
	);
};

export default ProductList;