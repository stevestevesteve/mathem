const productByName = name => require('../src/data/products.json').find(product => product.name === name);

module.exports = { productByName };
